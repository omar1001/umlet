package us.quantum42.umlet;

import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.facebook.ads.*;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

public class AndroidLauncher extends AndroidApplication implements Communicate {

	private InterstitialAd interstitialAd;
	private int adShowCount = 0;

	private void loadInterstitialAd() {
		interstitialAd = new InterstitialAd(this, "241064036416093_241065943082569");
		interstitialAd.setAdListener(new InterstitialAdListener() {
			@Override
			public void onInterstitialDisplayed(Ad ad) {
			}

			@Override
			public void onInterstitialDismissed(Ad ad) {
				interstitialAd.loadAd();
			}

			@Override
			public void onError(Ad ad, AdError adError) {
			}

			@Override
			public void onAdLoaded(Ad ad) {
			}

			@Override
			public void onAdClicked(Ad ad) {
			}

			@Override
			public void onLoggingImpression(Ad ad) {
			}
		});
		interstitialAd.loadAd();
	}

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		config.numSamples = 3;
		//initialize(new UmletGame(), config);
		View view = getLayoutInflater().inflate(R.layout.android_launcher, null);
		FrameLayout gameView = (FrameLayout) view.findViewById(R.id.game_view);
		gameView.addView(initializeForView(new UmletGame(this), config));
		setContentView(view);
		loadInterstitialAd();
	}

	@Override
	protected void onDestroy() {
		if (interstitialAd != null) {
			interstitialAd.destroy();
		}
		super.onDestroy();
	}

	@Override
	public void playerDied() {
		if(interstitialAd.isAdLoaded()) {
			if (adShowCount++ % 3 == 0) {
				interstitialAd.show();
			}
		}
	}

	@Override
	public void setHighScore(final int score) {

		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				((TextView) findViewById(R.id.highscore_text)).setText("Top: " + score);
			}
		});
	}

	@Override
	public void setCurrentScore(final int currentScore) {

		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				((TextView) findViewById(R.id.current_score_text)).setText("" + currentScore);
			}
		});
	}


	@Override
	protected void onResume() {
		super.onResume();
	}
}
