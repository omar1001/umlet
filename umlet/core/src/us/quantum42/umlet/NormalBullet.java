package us.quantum42.umlet;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * Created by omar on 6/16/17.
 * Hello
 */

class NormalBullet extends Bullet {
    private float radius;
    private int hitMultiplyer = 1;
    private boolean ballScoreDraw = false;
    private Vector2 ballCenterForScoreDraw;
    private float ballScoreDrawTime = 0;
    private BitmapFont font;
    private SpriteBatch batch;



    NormalBullet(Viewport viewport, Vector2 start, Vector2 end, FallingBalls fallingBalls) {
        super(viewport, start, end, fallingBalls);
        this.radius = Constants.BULLET_RADIUS_RATIO_TO_MIN_DIMENSION * Math.min(viewport.getScreenHeight(), viewport.getScreenWidth());
        batch = new SpriteBatch();
        font = new BitmapFont();
        font.setColor(Color.BLACK);
        font.getData().setScale(2, 2);
    }

    @Override
    void update(float delta) {
        time += delta;

        //there is a problem here!!
        // it's not a general velocity, this velocity depends on the difference between end and start
        // thats not acceptable, as if there is two balls moving regardless the reason at the same
        // start but different end parallel to each others, both of them will reach the final destination
        // at the same time that's not realistic velocity it should be constant velocity
        position.x = ((time / Constants.NORMAL_BULLET_TIME_LIMIT) * (end.x - start.x)) + start.x;
        position.y = ((time / Constants.NORMAL_BULLET_TIME_LIMIT) * (end.y - start.y)) + start.y;

//        if (end.x == start.x) {
//            Gdx.app.log("----------X", "X == X bro, end: " + end + ", start: " + start + ", position: " + position);
//        } if (end.y == start.y) {
//            Gdx.app.log("----------Y", "Y == Y bro, start.y: " + start.x);
//        }

        fallingBalls.balls.begin();
        for (int i=0; i < fallingBalls.balls.size; i++) {
            Ball ball = fallingBalls.balls.get(i);
            if (this.position.dst(ball.position) <= this.radius + ((NormalBall)ball).radius) {
                if (!fallingBalls.hitBall(i)) {
                    //TODO: here make the animation that the normal bullet do to the ball,
                    //  todo: if the ball didn't send false, this is for a case of that the ball
                    //  todo: might be a bonus for something.
                }
                collide(ball);
                fallingBalls.balls.removeIndex(i);
            }
        }
        fallingBalls.balls.end();
        if (ballScoreDraw) {
            ballScoreDrawTime += delta;
            if (ballScoreDrawTime > Constants.BALL_SCORE_DRAW_TIME_LIMIT) {
                ballScoreDraw = false;
                ballScoreDrawTime = 0f;
            }
        }
    }


    @Override
    void render(ShapeRenderer shapeRenderer) {
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(Constants.NORMAL_BULLET_COLOR);
        shapeRenderer.circle (
                this.position.x,
                this.position.y,
                this.radius
        );
        shapeRenderer.end();
        if (ballScoreDraw) {
            batch.begin();
            font.draw(batch, String.valueOf(hitMultiplyer/2), ballCenterForScoreDraw.x, ballCenterForScoreDraw.y); //you can change the position as you like
            batch.end();
        }
    }


    private void collide (Ball ball) {
        //reset time
        time = 0f;
        PlayScreen.normalBallHitSound.play(1.0f);
        //create new virtual circle object & get the new end of the ball.
        VirtualCircle virtualCircle = new VirtualCircle(viewport.getScreenHeight());
        virtualCircle.update(position, ball.position);
        start = position.cpy();
        end = virtualCircle.aimingEndPoint.cpy();
        ballScoreDraw = hitMultiplyer != 1;
        ballCenterForScoreDraw = ball.position;
        UmletGame.currentScore += hitMultiplyer;
        hitMultiplyer *= 2;


    }



}
