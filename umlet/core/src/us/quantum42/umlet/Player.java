package us.quantum42.umlet;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * Created by omar on 5/29/17.
 * Hello
 *
 * two stages:
 *      1> drag the aiming.
 *      2> shot the bullet.
 */

class Player implements InputProcessor{

    private Viewport viewport;
    static Vector2 center;
    static float touchCircleRadius;
    private int pointerOnFocus = -1; // -1 means that it's not aiming other wise is the id of the pointer that's aiming
    private Vector2 aimingPoint;
    private boolean AIMING = false;
    private VirtualCircle virtualCircle; // player bullets object uses this object as it's sent by reference, so don't use this object for other than aiming.
    private PlayerBullets playerBullets;
    FallingBalls fallingBalls;


    Player (Viewport viewport, FallingBalls fallingBalls) {
        this.viewport = viewport;
        this.fallingBalls = fallingBalls;
        init();
    }

    void init() {
        resize();
    }

    //update the center & the radius of the player because it's relative to the screen size.
    void resize() {
        center = new Vector2(0, 0);

//        center = new Vector2(
//                viewport.getScreenWidth() * Constants.PLAYER_CENTER_X_TO_SCREEN_WIDTH_RATIO,
//                viewport.getScreenHeight() * Constants.PLAYER_CENTER_Y_TO_SCREEN_HEIGHT_RATIO
//        );
        touchCircleRadius = Math.min (viewport.getScreenHeight(), viewport.getScreenWidth())
                * Constants.PLAYER_RADIUS_TOUCH_CIRCLE_RATIO_TO_MIN_DIMENSION;
        virtualCircle = new VirtualCircle(Math.max(viewport.getScreenWidth(), viewport.getScreenHeight()));
        playerBullets = new PlayerBullets(viewport, fallingBalls);
        playerBullets.init(virtualCircle);
    }

    void update (float delta) {
        //  call update in the bullet if in shooting mode.
        //  update aiming line if in aiming mode.
        playerBullets.update(delta);
    }

    void render (ShapeRenderer shapeRenderer) {

        Gdx.gl.glLineWidth(3);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);

        //bounds of input drag
        shapeRenderer.setColor(Constants.PLAYER_GUN_AIMING_BORDER_COLOR);
        shapeRenderer.circle(
                center.x,
                center.y,
                touchCircleRadius,
                128
        );


        //gun base.
        shapeRenderer.setColor(Constants.PLAYER_GUN_BASE_COLOR);
        shapeRenderer.circle(
                center.x,
                center.y,
                touchCircleRadius / 3,
                128

        );

        if (AIMING) {
            // draw the aiming line.
            shapeRenderer.setColor(Constants.PLAYER_AIMING_LINE_COLOR);
            shapeRenderer.line(
                    center,
                    virtualCircle.aimingEndPoint
            );
        }

        shapeRenderer.end();

        playerBullets.render(shapeRenderer);

    }

    // According game story of continuous shooting while aiming:
    //  this method is will call the shooting method in player bullets object
    //  shoot method will terminate shooting by calling a method in player bullets object.
    private void setAiming (float x, float y) {
        aimingPoint = viewport.unproject(new Vector2(x, y));
        if (aimingPoint.dst(center) <= touchCircleRadius) {
            return;
        }
        // check if the point is outside the touch circle then handle the aiming.
        AIMING = true;
        virtualCircle.update(center, aimingPoint);
        playerBullets.setContinuousShootingMode(true);
    }

    // This method is for shooting terminator.
    private void shoot() {
        AIMING = false;

        //check if the aiming point is outside the touch circle.
        if (aimingPoint.dst(center) <= touchCircleRadius) {
            return;
        }
        playerBullets.setContinuousShootingMode(false);
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if (pointerOnFocus == -1) {
            playerBullets.setContinuousShootingMode(false);
            playerBullets.init(virtualCircle);
            Vector2 point = viewport.unproject(new Vector2(screenX, screenY)); // we don't use unproject because we already using screenviewport
            pointerOnFocus = pointer;
            center = point;
            /*
            if (point.dst(center) <= touchCircleRadius) {
                pointerOnFocus = pointer;
            } else {
                pointerOnFocus = -1;
            }
            */
        }
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        //shot if pointer on focus is not -1, & set it to -1.
        if (pointer == pointerOnFocus) {
            setAiming(screenX, screenY);
            shoot();
            pointerOnFocus = -1;
        }
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if (pointerOnFocus == pointer) {
            setAiming(screenX, screenY);
        }
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

}
