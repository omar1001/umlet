package us.quantum42.umlet;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Sound;

public class UmletGame extends Game {
//	SpriteBatch batch;
//	Texture img;
	public static int highScore = 0;
	public static int currentScore = 0;
	private static int prevCurrentScore = 0;

	Preferences prefs;

	Communicate communicate;
	public UmletGame(Communicate communicate) {
		this.communicate = communicate;
	}

	@Override
	public void create () {
//		batch = new SpriteBatch();
//		img = new Texture("badlogic.jpg");
		setScreen(new PlayScreen(communicate));
		prefs = Gdx.app.getPreferences("u");
		highScore = prefs.getInteger(Constants.HIGH_SCORE, 0);
		communicate.setHighScore(highScore);
	}

	@Override
	public void render () {
//		Gdx.gl.glClearColor(1, 0, 0, 1);
//		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
//		batch.begin();
//		batch.draw(img, 0, 0);
//		batch.end();
		super.render();

		if (prevCurrentScore != currentScore) {
			communicate.setCurrentScore(currentScore);
			prevCurrentScore = currentScore;
			if (currentScore > highScore) {
				highScore = currentScore;
				communicate.setHighScore(currentScore);
				prefs.putInteger(Constants.HIGH_SCORE, highScore);
				prefs.flush();
			}
		}
	}

	@Override
	public void dispose () {
//		batch.dispose();
//		img.dispose();
		PlayScreen.playerLostSound.dispose();
		PlayScreen.normalBallHitSound.dispose();
		super.dispose();
	}
}
