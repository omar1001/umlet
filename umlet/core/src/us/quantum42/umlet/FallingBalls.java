package us.quantum42.umlet;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.DelayedRemovalArray;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.Random;

/**
 * Created by omar on 5/29/17.
 * Hello
 */

class FallingBalls {

    DelayedRemovalArray <Ball> balls;
    Viewport viewport;
    private float interval;
    private float intervalRandomLimit;

    private Random random;

    FallingBalls (Viewport viewport) {
        this.viewport = viewport;
        init();
    }

    void init () {
        balls = new DelayedRemovalArray<Ball>();
        interval = 0;
        random = new Random();
        resetIntervalRandom();
    }

    void update (float delta) {
        if ((interval += delta) > intervalRandomLimit) {
            balls.add(new NormalBall(viewport));
            resetIntervalRandom();
        }

        balls.begin();
        for (int i=0; i<balls.size; i++) {
            balls.get(i).update(delta);
            if (balls.get(i).madePlayerLose()) {// here I need to check if the ball hit the player circle
                balls.removeIndex(i);
                PlayScreen.playerLost = true;
            }
        }
        balls.end();
    }

    void render (ShapeRenderer shapeRenderer) {
        for (Ball ball: balls) {
            ball.render(shapeRenderer);
        }
    }

    boolean hitBall(int i) {
        // this means that the falling ball didn't handle the hit, the only case is if this ball is a bonus & needs to handle its own hit animation.
        return false;
    }

    private void resetIntervalRandom() {

        intervalRandomLimit = Constants.FALLING_BALLS_RANDOM_LIMIT_FACTOR;//random.nextFloat() * Constants.FALLING_BALLS_RANDOM_LIMIT_FACTOR;// "kol ad eh bynzel" btet3'ar
        interval = 0;
    }
}
