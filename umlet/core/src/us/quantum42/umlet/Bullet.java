package us.quantum42.umlet;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * Created by omar on 5/29/17.
 * Hello
 */

public class Bullet {

    Viewport viewport;
    Vector2 start;
    Vector2 end;
    Vector2 position;
    FallingBalls fallingBalls;
    float time;

    Bullet(Viewport viewport, Vector2 start, Vector2 end, FallingBalls fallingBalls) {
        this.viewport = viewport;
        this.start = start;
        this.end = end;
        this.position = start.cpy();
        this.fallingBalls = fallingBalls;
        time = 0f;
    }

    void update (float delta) {

    }

    void render (ShapeRenderer shapeRenderer) {

    }

    boolean isTotallyOutsideCircle(float radius) {
        return position.dst(start) > radius;
    }
}
