package us.quantum42.umlet;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * Created by omar on 5/29/17.
 * Hello
 */

class PlayScreen extends ScreenAdapter {

    private Viewport viewport;
    private ShapeRenderer shapeRenderer;
    private Player player;
    private FallingBalls balls;
    static boolean playerLost = false;
    static Sound playerLostSound;
    static Sound normalBallHitSound;
    static float timeSinceGameStart = 0f;
    Communicate communicate;

    PlayScreen (Communicate communicate) {
        this.communicate = communicate;
    }

    @Override
    public void show() {
        viewport = new ScreenViewport();
        shapeRenderer = new ShapeRenderer();
        shapeRenderer.setAutoShapeType(true);

        balls = new FallingBalls(viewport);

        player = new Player(viewport, balls);
        Gdx.input.setInputProcessor(player);
        playerLostSound = Gdx.audio.newSound(Gdx.files.internal("die.mp3"));
        normalBallHitSound = Gdx.audio.newSound(Gdx.files.internal("hit.mp3"));
    }

    @Override
    public void render(float delta) {
        timeSinceGameStart += delta;
        //updating
        viewport.apply();
        Gdx.gl.glClearColor(
                Constants.MAIN_SCREEN_BACKGROUND_R,
                Constants.MAIN_SCREEN_BACKGROUND_G,
                Constants.MAIN_SCREEN_BACKGROUND_B,
                1
        );
        //Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT | (Gdx.graphics.getBufferFormat().coverageSampling?GL20.GL_COVERAGE_BUFFER_BIT_NV:0));


        shapeRenderer.setProjectionMatrix(viewport.getCamera().combined);
        if (playerLost) {
            playerLostSound.play(1.0f);
            resetGame();
            communicate.playerDied();
            playerLost = false;
        }

        player.update(delta);
        player.render(shapeRenderer);

        balls.update(delta);
        balls.render(shapeRenderer);
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height, true);
        resetGame();
    }

    private void resetGame() {
        timeSinceGameStart = 0f;
        player.resize();
        balls.init();
        UmletGame.currentScore = 0;

    }

    @Override
    public void hide() {
        shapeRenderer.dispose();
    }
}
