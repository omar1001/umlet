package us.quantum42.umlet;


import com.badlogic.gdx.math.Vector2;



/**
 * Created by omar on 6/2/17.
 * Hello
 *
 * this class is to calculate the end point of the aiming which will be on the virtual circle
 * that's outside the screen of the game.
 *      first we get the create object from StraightLine class to help in calculating the
 *      constants of the str.line & to detect if the line is vertical
 * we calculate the end point of aiming by getting the intersection between the straight line
 * of aiming and the virtual circle, there will be two points on the circle though, but we choose
 * the closest to the center of the player than the aiming point, because the aiming line will
 * be rendered between the center of the player and the point on the virtual circle,
 * also the same things happen to the shooting bullet, which will acquire an end point to
 * make the velocity of it to it's direction.
 *
 *
 * I guess !! XDD
 */

class VirtualCircle {
    Vector2 center;
    private Vector2 aimingPoint;
    float radius;
    private Vector2 firstPoint; // root #1
    private Vector2 secondPoint; // root #2
    StraightLine aimingLine;
    Vector2 aimingEndPoint;


    VirtualCircle (float radius) {
        this.radius = radius;
        aimingLine = new StraightLine();
        aimingEndPoint = new Vector2();
        firstPoint = new Vector2();
        secondPoint = new Vector2();
    }

    void update(Vector2 center, Vector2 aimingPoint) {
        this.center = center;
        this.aimingPoint = aimingPoint;
        //update the aiming end point.
        calculate();
    }

    private void calculate () {
        /*
        TODO: Construct the straight line.
        TODO: Calculate a, b & c (NOTE: skip this part use x1 = x2 = cx if it's vertical line)
        TODO: Calculate the roots, using the quadratic formula for x then straight line equation for y
        TODO: Choose the aiming end point
        */
        //      Get x1 & x2

        if (aimingLine.update(center, aimingPoint).isVertical) {
            // TODO: handle vertical line.
            firstPoint.x = center.x;
            secondPoint.x = center.x;
        } else {
            final float M = aimingLine.slope;
            final float C = aimingLine.c;
            final float Cx = center.x;
            final float Cy = center.y;
            final float r = radius;

            final float a = 1f + (M * M);
            final float b = 2f * (M * (C - Cy) - Cx);
            final float c = (Cx * Cx) + (C * C) - (2f * Cy * C) - (r * r);//(Cx * Cx) + (C - Cy) * (C - Cy) + (r * r);

            Vector2 roots = quadraticFormula(a, b, c);
            firstPoint.x = roots.y;
            secondPoint.x = roots.x;
        }

        if (aimingLine.isVertical) {
            firstPoint.y = (center.y - radius);
            secondPoint.y = radius + center.y;
        } else {
            firstPoint.y = aimingLine.getYFromX(firstPoint.x);
            secondPoint.y = aimingLine.getYFromX(secondPoint.x);
        }

        //assign the end point on the circle to the one closest to the center not the aiming point
        if (aimingPoint.dst(firstPoint) < aimingPoint.dst(secondPoint)) {
            aimingEndPoint.x = secondPoint.x;//for continuous bullet on the aiming line
            aimingEndPoint.y = secondPoint.y;
//            aimingEndPoint = secondPoint.cpy(); //for bullets not on the aiming line

        } else {
            aimingEndPoint.x = firstPoint.x;
            aimingEndPoint.y = firstPoint.y;
//            aimingEndPoint = firstPoint.cpy();

        }
    }

    private static Vector2 quadraticFormula (final float a, final float b, final float c) {
        final float underSquareRoot = (b * b) - (4f * a * c);
        final float leftTerm = (-1f * b) / (2f * a);
        final float rightTerm = ((float) Math.sqrt(underSquareRoot)) / (2f * a);

        return new Vector2(
                leftTerm + rightTerm,
                leftTerm - rightTerm
        );
    }
}
