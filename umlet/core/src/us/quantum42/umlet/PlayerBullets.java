package us.quantum42.umlet;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.DelayedRemovalArray;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * Created by omar on 6/13/17.
 * Hello
 */

class PlayerBullets {

    Viewport viewport;
    DelayedRemovalArray <Bullet> bullets;
    private boolean continuousShootingMode = false;
    VirtualCircle virtualCircle;
    private float interval;
    FallingBalls fallingBalls;

    PlayerBullets (Viewport viewport, FallingBalls fallingBalls) {
        this.fallingBalls = fallingBalls;
        this.viewport = viewport;
        bullets = new DelayedRemovalArray<Bullet>();
    }

    void init(VirtualCircle virtualCircle) {
        this.virtualCircle = virtualCircle;
        interval = Constants.INTERVAL_LIMIT_NORMAL_BULLET;
        bullets.clear();
    }

    void update (float delta) {
        if (continuousShootingMode) {
            //here we will check for the type of the ball that will be shoot
            // for example it might be a laser so the interval variable is not in use
            //
            if ((interval += delta) >= Constants.INTERVAL_LIMIT_NORMAL_BULLET) {
                bullets.add(new NormalBullet(viewport, virtualCircle.center, virtualCircle.aimingEndPoint, fallingBalls));
                interval = 0;
            }
        }

        bullets.begin();
        for (int i=0; i<bullets.size; i++) {
            bullets.get(i).update(delta);
            if (bullets.get(i).isTotallyOutsideCircle(virtualCircle.radius)) {
                bullets.removeIndex(i);
            }
        }
        bullets.end();
    }

    void render (ShapeRenderer shapeRenderer) {
        if (continuousShootingMode) {
            // TODO: CHECK IF ANY UPDATED TASK NEEDS TO BE SET IF THE SHOOTING MODE IS ON.
        }
        for (Bullet bullet: bullets) {
            bullet.render(shapeRenderer);
        }
    }

    void setContinuousShootingMode (boolean flag) {
        this.continuousShootingMode = flag;
        if (!flag) {
            interval = Constants.INTERVAL_LIMIT_NORMAL_BULLET;
        }
    }

}
