package us.quantum42.umlet;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.Random;

/**
 * Created by omar on 6/16/17.
 * Hello
 */

class NormalBall extends Ball {

    float radius;

    NormalBall(Viewport viewport) {
        super(viewport);
    }

    @Override
    void init() {
        super.init();
        radius = Math.min(viewport.getScreenHeight(), viewport.getScreenWidth())
                * Constants.BALL_RADIUS_RATIO_TO_MIN_DIMENSION;
        float ballX = new Random().nextFloat() * viewport.getScreenWidth();
        if (ballX - radius < 0) {
            ballX = radius;
        } else if (ballX + radius > viewport.getScreenWidth()) {
            ballX = viewport.getScreenWidth() - radius;
        }
        position = new Vector2(
                ballX,
                viewport.getScreenHeight() + radius
        );
    }

    @Override
    void update(float delta) {
        super.update(delta);
    }

    @Override
    void render(ShapeRenderer shapeRenderer) {
        shapeRenderer.setColor(Constants.NORMAL_BALL_COLOR);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.circle(
                position.x,
                position.y,
                radius,
                32
        );
        shapeRenderer.end();
    }

    @Override
    boolean isTotallyOutSideScreen() {
        // Not Generic
        return (position.y + radius < 0);


        //Generic:
//        if (position.x - radius > viewport.getScreenWidth() || position.x + radius < 0) {
//            return true;
//        } else if (position.y - radius > viewport.getScreenHeight() || position.y + radius < 0) {
//            return true;
//        }
//        return false;
    }

    @Override
    public boolean madePlayerLose() {
        return (isTotallyOutSideScreen() || (Player.center.dst(this.position) <= (Player.touchCircleRadius + this.radius)));
    }
}
