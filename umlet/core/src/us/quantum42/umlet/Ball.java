package us.quantum42.umlet;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * Created by omar on 5/29/17.
 * Hello
 */

class Ball {

    Viewport viewport;
    Vector2 position;
    private Vector2 velocity;

    Ball (Viewport viewport) {
        this.viewport = viewport;
        position = new Vector2();
        velocity = new Vector2();
        init();
    }

    void init () {
        //velocity.y = - viewport.getScreenHeight() * Constants.NORMAL_BALL_INITIAL_VELOCITY_RATIO_TO_HEIGHT;
        float h = viewport.getScreenHeight();
        float initialVelocityY = h * Constants.NORMAL_BALL_INITIAL_VELOCITY_RATIO_TO_HEIGHT;
        velocity.y = - (((h - initialVelocityY) * Math.min((PlayScreen.timeSinceGameStart / Constants.NORMAL_BALL_FULL_SPEED_INTERVAL_IN_SECONDS), 1f)) + initialVelocityY);
        velocity.x = 0f;
    }

    void update (float delta) {
//        velocity.y = velocity.y
        position.y += velocity.y * delta;
        position.x += velocity.x * delta;
    }

    void render (ShapeRenderer shapeRenderer) {

    }

    boolean isTotallyOutSideScreen () {
        return true;
    }

    boolean isNormalBulletCollided (float bulletRadius, float bulletPosition) {
        return false;
    }

    public boolean madePlayerLose() {
        return true;
    }
}
