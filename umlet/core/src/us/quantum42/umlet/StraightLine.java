package us.quantum42.umlet;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by omar on 5/30/17.
 * Hello
 *
 * this class is to handle a straight line equation calculations
 * * calculates points x & y on the straight line
 *
 * TODO: handle vertical slope & horizontal slope
 */

class StraightLine {

    //       y = Mx + C

    private Vector2 point1;
    private Vector2 point2;
    public float c;
    public float slope;
    public boolean isVertical = false;


    public StraightLine(Vector2 point1, Vector2 point2) {
        update(point1, point2);
    }
    public StraightLine() {}

    public StraightLine update(Vector2 point1, Vector2 point2) {
        this.point1 = point1;
        this.point2 = point2;
        isVertical = point1.x == point2.x;

        if (isVertical) {
            return this;
        }

        slope = (point1.y - point2.y) / (point1.x - point2.x);

        // taking any point to calculate c, where c = y - mx
        c = point1.y - (slope * point1.x);
        return this;
    }

    // CHECK & HANDLE if the returned value is infinity so the slope is 0 (Horizontal Line)
    public float getXFromY(float y) {
        if (isVertical) {
            return point1.x;
        }
        if (slope == 0f) {
            return Constants.INFINITY;
        }
        return (y - c) / slope;
    }

    // CHECK & HANDLE if the returned value is infinity so the slope is infinity (Vertical Line)
    public float getYFromX (float x) {
        if (isVertical) {
            return Constants.INFINITY;
        }
        if (slope == 0) {
            return point1.y;
        }
        return (slope * x + c);
    }

}
